import unittest  

from model.season_row import SeasonRow
from model.team import Team

class TestSeason(unittest.TestCase):  
            
    def test_bet_score(self):
        seasonRow = SeasonRow(Team('a'))
        self.assertEqual(0, seasonRow.bet_score())
        
        seasonRow.local_score = 1
        seasonRow.visitant_score = 1
        self.assertEqual(2, seasonRow.bet_score())

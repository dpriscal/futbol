import unittest  

from model.teams_finder import TeamsFinder
from model.gameweek import Gameweek
from model.season import Season

class TestSeason(unittest.TestCase):  
            
    def setUp(self):
        self.teamsFinder = TeamsFinder([])

    
    def testLastGameweek(self):
        season = Season(self.teamsFinder)
        self.assertEqual(None, season.lastGameweek())
        
        gameweek1 = Gameweek('1', [], self.teamsFinder)
        season.addGameweek(gameweek1)        
        self.assertEqual(gameweek1.name, season.lastGameweek().name)

        gameweek2 = Gameweek('2', [], self.teamsFinder)
        season.addGameweek(gameweek2)        
        self.assertEqual(gameweek2.name, season.lastGameweek().name)
        
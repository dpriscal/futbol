from model.teams_finder import TeamsFinder
from model.team import Team
from model.gameweek import Gameweek
from model.season import Season
from model.both_teams_score_strategy import BothTeamsScoreStrategy
from model.match_filter import MatchFilter
from model.printer import Printer

import requests
from bs4 import BeautifulSoup
import sys

SL = "SL"
SV = 'SV'
RL = 'RL'
RV = 'RV'


def to_int(str):
    try:
        return int(str)
    except:
        return None

def get_score(resultado):
    try:
        score_split = resultado.get_text().split(' - ')
        local_score = to_int(score_split[0].strip())
        visitant_score = to_int(score_split[1].strip())
    
        return [local_score, visitant_score]
    except:
        return [None, None]


def get_teams(resultado):
    teams = resultado['title']
    teams_split = teams.split(' - ')
    local_team = teams_split[0]
    visitant_team = teams_split[1].replace(' en directo', '')

    return [local_team, visitant_team]


def get_all_teams(soup, resultados):
    all_teams = []
    for resultado in resultados:
        teams = get_teams(resultado)
        all_teams.append(Team(teams[0]))
        all_teams.append(Team(teams[1]))
    return TeamsFinder(all_teams)

def get_map_results(soap, CURRENT_GAMEWEEK, all_teams):
    season = Season(all_teams)
    jornada_st = ''
    for i in range(1,CURRENT_GAMEWEEK + 1):
        jornada_st = 'jornada-' + str(i)
        jornada = soup.find_all("div", attrs={"id": jornada_st})
        resultados = jornada[0].find_all("a", attrs={"class": "resultado"})
        gameweek = Gameweek(jornada_st, [], all_teams)
    
        for resultado in resultados:
            teams = get_teams(resultado)
            score = get_score(resultado)
            local_team = teams[0]
            visitant_team = teams[1]
            local_score = score[0]
            visitant_score = score[1]
            gameweek.add_match_with_score(local_team, visitant_team, local_score, visitant_score)
        
        if i == CURRENT_GAMEWEEK - 1:
            gameweek.finished = False
        season.addGameweek(gameweek)
        
    return season
        
category = 2
if len(sys.argv) > 1:
    category = sys.argv[1]

if category == '1':
    CURRENT_GAMEWEEK = 21
    url = 'https://resultados.as.com/resultados/futbol/primera/calendario/'
elif category == '2' :
    CURRENT_GAMEWEEK = 25
    url = 'https://resultados.as.com/resultados/futbol/segunda/calendario/'
elif category == '3' :
    CURRENT_GAMEWEEK = 25
    url = 'https://resultados.as.com/resultados/futbol/inglaterra/calendario'
else:
    CURRENT_GAMEWEEK = 21
    url = 'https://resultados.as.com/resultados/futbol/italia/calendario/'


response = requests.get(url)
soup = BeautifulSoup(response.text, 'html.parser')
jornada = soup.find_all("div", attrs={"id": "jornada-1"})
resultados = jornada[0].find_all("a", attrs={"class": "resultado"})
all_teams = get_all_teams(soup, resultados)

season = get_map_results(soup, CURRENT_GAMEWEEK, all_teams)
strategy = BothTeamsScoreStrategy(season)
betMetrics = strategy.run(CURRENT_GAMEWEEK - 1)

printer = Printer()

betMetrics.print(printer)
printer.print(strategy.maxDrawDown(CURRENT_GAMEWEEK - 1))

matchFilter = MatchFilter(season)
filteredSeason = matchFilter.run()
strategy = BothTeamsScoreStrategy(filteredSeason)
betMetrics = strategy.run(CURRENT_GAMEWEEK - 1)

printer.print('**************')
betMetrics.print(printer)
printer.print(strategy.maxDrawDown(CURRENT_GAMEWEEK - 1))

matchFilter.printMatchesToBet(printer)
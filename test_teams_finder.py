import unittest  

from model.teams_finder import TeamsFinder
from model.team import Team


class TestTeamsFinder(unittest.TestCase):  

    def test_find(self):
        a = Team('a')
        b = Team('b')
        
        teams = [a,b]
        
        teamsFinder = TeamsFinder(teams)
        self.assertEqual(None, teamsFinder.find('x'))

        teamsFinder = TeamsFinder(teams)
        self.assertEqual('a', teamsFinder.find('a').name)

import unittest

from model.season import Season
from model.gameweek import Gameweek
from model.both_teams_score_strategy import BothTeamsScoreStrategy
from model.team import Team
from model.teams_finder import TeamsFinder

class TestBothTeamsScoreStrategy(unittest.TestCase):
    def setUp(self):
        a = Team('a')
        b = Team('b')
        c = Team('c')
        d = Team('d')
        e = Team('e')
        f = Team('f')
        g = Team('g')
        h = Team('h')
        teams = [a,b,c,d,e,f,g,h]

        self.teamsFinder = TeamsFinder(teams)

    
    def testRun(self):
        season = Season(self.teamsFinder)
        gameweek1 = Gameweek('1', [], self.teamsFinder)
        gameweek1.add_match_with_score('a','b', 0, 0)
        gameweek1.add_match_with_score('c','d', 1, 1)
        season.addGameweek(gameweek1)

        strategy = BothTeamsScoreStrategy(season)
        betMetrics = strategy.run(1)
        self.assertEqual(1, betMetrics.wins)
        self.assertEqual(1, betMetrics.faults)

        gameweek2 = Gameweek('2', [], self.teamsFinder)
        gameweek2.add_match_with_score('a','c', 1, 0)
        gameweek2.add_match_with_score('b','d', 1, 1)
        season.addGameweek(gameweek2)

        betMetrics = strategy.run(2)
        self.assertEqual(2, betMetrics.wins)
        self.assertEqual(2, betMetrics.faults)

    def testMaxDrawDown(self):
        season = Season(self.teamsFinder)
        gameweek1 = Gameweek('1', [], self.teamsFinder)
        gameweek1.add_match_with_score('a','b', 0, 0)
        gameweek1.add_match_with_score('c','d', 1, 0)
        season.addGameweek(gameweek1)

        strategy = BothTeamsScoreStrategy(season)
        self.assertEqual(2, strategy.maxDrawDown(1))

        gameweek2 = Gameweek('2', [], self.teamsFinder)
        gameweek2.add_match_with_score('a','c', 1, 0)
        gameweek2.add_match_with_score('b','d', 0, 0)
        season.addGameweek(gameweek2)

        self.assertEqual(4, strategy.maxDrawDown(2))

        gameweek3 = Gameweek('3', [], self.teamsFinder)
        gameweek3.add_match_with_score('a','c', 1, 1)
        gameweek3.add_match_with_score('b','d', 1, 1)
        season.addGameweek(gameweek3)

        self.assertEqual(4, strategy.maxDrawDown(2))

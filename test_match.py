import unittest  

from model.team import Team
from model.match import Match

class TestMatch(unittest.TestCase):

    def testUpdateFinished(self):
        local = Team('local')
        visitant = Team('visitant')
        
        match = Match(local, visitant, None, None)
        self.assertEqual(False, match.finished)
        
        match = Match(local, visitant, 0, 0)
        self.assertEqual(True, match.finished)

    def testScoreBothTeams(self):
        local = Team('local')
        visitant = Team('visitant')
        
        match = Match(local, visitant, None, None)
        self.assertEqual(False, match.scoreBothTeams())
        
        match = Match(local, visitant, 1, 0)
        self.assertEqual(False, match.scoreBothTeams())

        match = Match(local, visitant, 1, 1)
        self.assertEqual(True, match.scoreBothTeams())

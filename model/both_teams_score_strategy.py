from model.bet_metric import BetMetric

class BothTeamsScoreStrategy:
    def __init__(self, season):
        self.season = season

    def maxDrawDown(self, gameweekNumber):
        maxDrawDown = 0
        for i in range(1,gameweekNumber + 1):
            maxDrawDown = max([maxDrawDown, self.run(i).drawDown()])
        return maxDrawDown
        
    def run(self, gameweekNumber):
        wins = 0
        faults = 0
        for g in self.season.gameweeks[0:gameweekNumber + 1]:
            gameweekWins = g.matchesWhereScoreBothTeams()
            gameweekFaults = len(g.matches) - gameweekWins
            wins += gameweekWins
            faults += gameweekFaults
        return BetMetric(wins, faults)

        
class TeamsFinder:
    def __init__(self,teams):
        self.teams = teams
    
    def find(self, name):
        for team in self.teams:  
            if team.name == name:
                return team

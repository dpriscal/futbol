class Season:
    def __init__(self, teamsFinder):
        self.gameweeks = []
        self.teamsFinder = teamsFinder

    def addGameweek(self, gameweek):
        self.gameweeks.append(gameweek)

    def lastGameweek(self):
        if self.gameweeks:
            return self.gameweeks[-1]
        else:
            return None

    def print(self, printer):
        for g in self.gameweeks:
            printer.print('')
            g.print(printer)
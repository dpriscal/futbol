from model.match import Match

class Gameweek:
    def __init__(self,name, matches, teamsFinder):
        self.name = name
        self.matches = matches
        self.teamsFinder = teamsFinder
        self.update_finished()
    
    def add_match(self, local_name, visitant_name):
        self.add_match_with_score(local_name, visitant_name, None, None)

    def add_match_with_score(self, local_name, visitant_name, local_score, visitant_score):
        local = self.teamsFinder.find(local_name)
        visitant = self.teamsFinder.find(visitant_name)
        self.matches.append(Match(local, visitant, local_score, visitant_score))
        self.update_finished()

    def update_finished(self):
        self.finished = True
        for match in self.matches:
            if not match.finished:
                self.finished = False
                return

    def matchesWhereScoreBothTeams(self):
            wins = 0
            for m in self.matches:
                if m.scoreBothTeams():
                    wins += 1
            return wins

    def print(self, printer):
        printer.print('Gameweek: ' + self.name)
        for match in self.matches:
            match.print(printer)
class BetMetric:
    def __init__(self, wins, faults):
        self.wins = wins
        self.faults = faults
    
    def print(self, printer):
        printer.print('Wins: ' + str(self.wins))
        printer.print('Faults: ' + str(self.faults))
        printer.print('DrawDown: ' + str(self.drawDown()))
        
    def drawDown(self):
        return self.faults - self.wins

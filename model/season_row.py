class SeasonRow:
    def __init__(self,team):
        self.team = team
        self.local_score = 0
        self.visitant_score = 0

    def bet_score(self):        
        return self.local_score + self.visitant_score

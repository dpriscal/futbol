class Match:
    def __init__(self,local_team, visitant_team, local_score, visitant_score):
        self.local_team=local_team
        self.visitant_team=visitant_team
        self.local_score = local_score
        self.visitant_score = visitant_score
        self.updateFinished()
    
    def updateFinished(self):
        self.finished = True
        if self.local_score is None or self.visitant_score is None:
            self.finished = False

    def scoreBothTeams(self):
        if (self.finished):
            return self.local_score > 0 and self.visitant_score > 0
        return False

    def print(self, printer):
        printer.print(self.local_team.name + ' vs ' + self.visitant_team.name + ': ' + str(self.local_score) + ' ' + str(self.visitant_score))

from model.season_row import SeasonRow
from model.season import Season
from model.gameweek import Gameweek

class MatchFilter:
    def __init__(self, season):
        self.season = season
        self.createSeasonRows()

    def createSeasonRows(self):
        self.seasonRows = []
        for team in self.season.teamsFinder.teams:
            self.seasonRows.append(SeasonRow(team))

    def run(self):
        season = Season(self.season.teamsFinder)
        gameweeks = self.season.gameweeks
        
        if len(gameweeks) <= 1:
            return season
        
        self.updateSeasonRows(gameweeks[0])
        
        for g in gameweeks[1:len(gameweeks)]:
            season.addGameweek(Gameweek(g.name, self.matchesToBet(g), g.teamsFinder))
            self.updateSeasonRows(g)
        return season

    def updateSeasonRows(self, gameweek):
        if gameweek.finished:
            for match in gameweek.matches:
                if match.finished:
                    local = match.local_team
                    visitant = match.visitant_team
                    localSeasonRow = self.findSeasonRow(local)
                    visitantSeasonRow = self.findSeasonRow(visitant)
                    localSeasonRow.local_score += match.local_score
                    visitantSeasonRow.visitant_score += match.visitant_score


    def printMatchesToBet(self, printer):
        for match in self.matchesToBet(self.season.lastGameweek()):
            printer.print(match.local_team.name + ' - ' + match.visitant_team.name + ': ' + str(self.betScoreFrom(match)))
    
    def matchesToBet(self, gameweek):
        matches_len = len(gameweek.matches)
        sorted_matches = sorted(gameweek.matches, key=lambda x: self.betScoreFrom(x), reverse=True)
        return sorted_matches[0:matches_len//2]

    def betScoreFrom(self,match):
        localSeasonRow = self.findSeasonRow(match.local_team)
        visitantSeasonRow = self.findSeasonRow(match.visitant_team)
        return localSeasonRow.bet_score() + visitantSeasonRow.bet_score()
    
    def findSeasonRow(self, team):
        for seasonRow in self.seasonRows:
            if seasonRow.team == team:
                return seasonRow

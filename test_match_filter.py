import unittest  

from model.season import Season
from model.team import Team
from model.teams_finder import TeamsFinder
from model.gameweek import Gameweek
from model.match_filter import MatchFilter

class TestMatchFilter(unittest.TestCase):  
    
    def setUp(self):
        a = Team('a')
        b = Team('b')
        c = Team('c')
        d = Team('d')
        e = Team('e')
        f = Team('f')
        g = Team('g')
        h = Team('h')
        teams = [a,b,c,d,e,f,g,h]

        self.teamsFinder = TeamsFinder(teams)
        
    def testConstructor(self):
        season = Season(self.teamsFinder)
        matchFilter = MatchFilter(season)
        
        self.assertEqual(8, len(matchFilter.seasonRows))
        self.assertEqual('a', matchFilter.seasonRows[0].team.name)
        
    def testfindSeasonRow(self):
        season = Season(self.teamsFinder)
        matchFilter = MatchFilter(season)

        seasonRow = matchFilter.findSeasonRow(self.teamsFinder.find('a'))
        self.assertEqual('a', seasonRow.team.name)
    
    def testupdateSeasonRow(self):
        season = Season(self.teamsFinder)
        matchFilter = MatchFilter(season)

        gameweek1 = Gameweek('1', [], self.teamsFinder)
        gameweek1.add_match_with_score('a','b', 1, 0)
        season.addGameweek(gameweek1)        
        matchFilter.updateSeasonRows(gameweek1)

        self.assertEqual(1, matchFilter.findSeasonRow(self.teamsFinder.find('a')).local_score)
        self.assertEqual(0, matchFilter.findSeasonRow(self.teamsFinder.find('a')).visitant_score)
        self.assertEqual(0, matchFilter.findSeasonRow(self.teamsFinder.find('b')).local_score)
        self.assertEqual(0, matchFilter.findSeasonRow(self.teamsFinder.find('b')).visitant_score)

        gameweek2 = Gameweek('2', [], self.teamsFinder)
        gameweek2.add_match_with_score('b','a', 1, 2)
        season.addGameweek(gameweek2)        
        matchFilter.updateSeasonRows(gameweek2)

        self.assertEqual(1, matchFilter.findSeasonRow(self.teamsFinder.find('a')).local_score)
        self.assertEqual(2, matchFilter.findSeasonRow(self.teamsFinder.find('a')).visitant_score)
        self.assertEqual(1, matchFilter.findSeasonRow(self.teamsFinder.find('b')).local_score)
        self.assertEqual(0, matchFilter.findSeasonRow(self.teamsFinder.find('b')).visitant_score)

                
    def testRun(self):
        season = Season(self.teamsFinder)
        matchFilter = MatchFilter(season)

        gameweek1 = Gameweek('1', [], self.teamsFinder)
        gameweek1.add_match_with_score('a','b', 0, 0)
        gameweek1.add_match_with_score('c','d', 0, 0)
        gameweek1.add_match_with_score('e','f', 1, 0)
        gameweek1.add_match_with_score('g','h', 1, 1)
        
        season.addGameweek(gameweek1)

        self.assertEqual(0, len(matchFilter.run().gameweeks))

        gameweek2 = Gameweek('2', [], self.teamsFinder)
        gameweek2.add_match('a','b')
        gameweek2.add_match('c','d')
        gameweek2.add_match('e','f')
        gameweek2.add_match('g','h')

        season.addGameweek(gameweek2)
        season = matchFilter.run()
        self.assertEqual(1, len(season.gameweeks))

        self.assertEqual('g', season.gameweeks[0].matches[0].local_team.name)
        self.assertEqual('h', season.gameweeks[0].matches[0].visitant_team.name)
        self.assertEqual('e', season.gameweeks[0].matches[1].local_team.name)
        self.assertEqual('f', season.gameweeks[0].matches[1].visitant_team.name)

import unittest  

from model.teams_finder import TeamsFinder
from model.team import Team
from model.match import Match
from model.gameweek import Gameweek

class TestGameweek(unittest.TestCase):  

    def setUp(self):
        a = Team('a')
        b = Team('b')
        c = Team('c')
        d = Team('d')
        e = Team('e')
        f = Team('f')
        g = Team('g')
        h = Team('h')
        teams = [a,b,c,d,e,f,g,h]

        self.teamsFinder = TeamsFinder(teams)
    
    def test_initial_value(self):
        local = Team('Salamanca')
        visitant = Team('Deportivo')
        
        match1 = Match(local, visitant, None, None)
        self.assertEqual(False, match1.finished)
        
        match2 = Match(local, visitant, 1, 1)
        self.assertEqual(True, match2.finished)
        
        matches = [match1,match2]
        
        gameweek = Gameweek('1', matches, None)
        self.assertEqual(False, gameweek.finished)
            
    def test_add_match(self):
        
        gameweek = Gameweek('1', [], self.teamsFinder)
        gameweek.add_match('a','b')

        self.assertEqual(1, len(gameweek.matches))
        self.assertEqual('a', gameweek.matches[0].local_team.name)
        self.assertEqual('b', gameweek.matches[0].visitant_team.name)
        
    def matchesWhereScoreBothTeams(self):
        gameweek = Gameweek('1', [], self.teamsFinder)
        self.assertEqual(0, gameweek.matchesWhereScoreBothTeams())

        gameweek.add_match_with_score('a','b', 1, 1)
        gameweek.add_match_with_score('c','d', 1, 0)
        self.assertEqual(1, gameweek.matchesWhereScoreBothTeams())
